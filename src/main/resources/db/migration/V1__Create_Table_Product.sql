create table if not exists product
(
    id bigint auto_increment
    primary key,
    deleted_at datetime null,
    description varchar(255) null,
    name varchar(255) null,
    price decimal(19,2) null
    )
    engine=InnoDB default charset=utf8;