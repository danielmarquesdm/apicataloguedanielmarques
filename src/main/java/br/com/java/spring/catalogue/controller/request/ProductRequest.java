package br.com.java.spring.catalogue.controller.request;

import br.com.java.spring.catalogue.model.Product;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ProductRequest {
    @NotEmpty
    private String name;
    @NotEmpty
    private String description;
    @NotNull
    private BigDecimal price;

    public Product toProduct() {
        Product product = new Product();

        product.setName(this.name);
        product.setDescription(this.description);
        product.setPrice(this.price);

        return product;
    }

    public Product toProduct(String id) {
        Product product = new Product();

        product.setId(Long.parseLong(id));
        product.setName(this.name);
        product.setDescription(this.description);
        product.setPrice(this.price);

        return product;
    }

}
