package br.com.java.spring.catalogue.service.impl;

import br.com.java.spring.catalogue.controller.request.ProductRequest;
import br.com.java.spring.catalogue.controller.response.ProductResponse;
import br.com.java.spring.catalogue.exception.InvalidRequestException;
import br.com.java.spring.catalogue.exception.ProductNotFoundException;
import br.com.java.spring.catalogue.model.Product;
import br.com.java.spring.catalogue.repository.ProductRepository;
import br.com.java.spring.catalogue.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public ProductResponse save(ProductRequest productRequest) throws InvalidRequestException {
        var isValid = this.positiveValueForPriceValidator(productRequest);

        if (!isValid) {
            throw new InvalidRequestException("Price should have a positive value");
        }

        return ProductResponse.toResponse(this.productRepository.save(productRequest.toProduct()));
    }

    private boolean positiveValueForPriceValidator(ProductRequest productRequest) {
        return productRequest.getPrice().compareTo(BigDecimal.ZERO) >= 0;
    }

    @Override
    public ProductResponse update(ProductRequest productRequest, String id) {
        final Product response = this.getProduct(Long.parseLong(id));
        return ProductResponse
                .toResponse(this.productRepository.save(productRequest.toProduct(response.getId().toString())));
    }

    @Override
    public ProductResponse findById(Long id) {
        return ProductResponse.toResponse(getProduct(id));
    }

    @Override
    public List<ProductResponse> findAll() {
        return this.productRepository.findAll().stream()
                .map(ProductResponse::toResponse).collect(Collectors.toList());
    }

    @Override
    public void remove(Long id) {
        Product product = this.getProduct(id);
        product.setDeletedAt(LocalDateTime.now());
        this.productRepository.save(product);
    }

    @Override
    public List<ProductResponse> searchBy(String q, BigDecimal minPrice, BigDecimal maxPrice) {
        return this.productRepository.searchBy(q, minPrice, maxPrice).stream()
                .map(ProductResponse::toResponse).collect(Collectors.toList());
    }

    private Product getProduct(Long id) {
        return this.productRepository.getOne(id).orElseThrow(
                () -> new ProductNotFoundException("No product found for this id")
        );
    }
}
