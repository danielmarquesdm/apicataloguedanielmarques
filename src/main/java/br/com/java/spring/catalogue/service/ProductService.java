package br.com.java.spring.catalogue.service;

import br.com.java.spring.catalogue.controller.request.ProductRequest;
import br.com.java.spring.catalogue.controller.response.ProductResponse;
import br.com.java.spring.catalogue.exception.InvalidRequestException;

import java.math.BigDecimal;
import java.util.List;


public interface ProductService {
    ProductResponse save(ProductRequest productRequest) throws InvalidRequestException;

    ProductResponse update(ProductRequest productRequest, String id);

    ProductResponse findById(Long id);

    List<ProductResponse> findAll();

    void remove(Long id);

    List<ProductResponse> searchBy(String q, BigDecimal minPrice, BigDecimal maxPrice);
}
