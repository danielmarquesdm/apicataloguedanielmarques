package br.com.java.spring.catalogue.repository.impl;


import br.com.java.spring.catalogue.model.Product;
import br.com.java.spring.catalogue.repository.ProductDynamicQueriesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class ProductRepositoryImpl implements ProductDynamicQueriesRepository {
    @PersistenceContext
    private EntityManager manager;

    @Override
    public List<Product> searchBy(String q, BigDecimal minPrice, BigDecimal maxPrice) {
        CriteriaBuilder builder = manager.getCriteriaBuilder();
        CriteriaQuery<Product> criteriaQuery = builder.createQuery(Product.class);

        Root<Product> root = criteriaQuery.from(Product.class);

        var predicates = new ArrayList<Predicate>();

        if (StringUtils.hasText(q)) {
            Predicate namePredicate = builder.like(root.get("name"), "%" + q + "%");
            Predicate descriptionPredicate = builder.like(root.get("description"), "%" + q + "%");
            Predicate orPredicate = builder.or(namePredicate, descriptionPredicate);
            predicates.add(orPredicate);
        }

        if (minPrice != null) {
            Predicate minPricePredicate = builder.greaterThanOrEqualTo(root.get("price"), minPrice);
            predicates.add(minPricePredicate);
        }

        if (maxPrice != null) {
            Predicate maxPricePredicate = builder.lessThanOrEqualTo(root.get("price"), maxPrice);
            predicates.add(maxPricePredicate);
        }

        if (predicates.isEmpty()) {
            return Collections.emptyList();
        }

        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        TypedQuery<Product> query = manager.createQuery(criteriaQuery);
        return query.getResultList();
    }
}
