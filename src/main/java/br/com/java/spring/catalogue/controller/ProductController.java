package br.com.java.spring.catalogue.controller;

import br.com.java.spring.catalogue.controller.request.ProductRequest;
import br.com.java.spring.catalogue.controller.response.ProductResponse;
import br.com.java.spring.catalogue.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.net.URI;
import java.util.List;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

@Api(value = "Product Endpoints", tags = {"Product Endpoints"})
@CrossOrigin
@RequestMapping(value = "/products")
@RestController
public class ProductController {
    @Autowired
    private ProductService productService;

    @ApiOperation(value = "Register a new product")
    @PostMapping
    public ResponseEntity<ProductResponse> save(@RequestBody @Valid ProductRequest productRequest,
                                                UriComponentsBuilder uriBuilder) {
        var response = this.productService.save(productRequest);
        URI uri = uriBuilder.path("/products/").buildAndExpand(response.getId()).toUri();

        return created(uri).body(response);
    }

    @ApiOperation(value = "Update a product by its id")
    @PutMapping(value = "/{id}")
    public ResponseEntity<ProductResponse> update(@RequestBody @Valid ProductRequest productRequest,
                                                  @PathVariable("id") String id) {
        return ok(this.productService.update(productRequest, id));
    }

    @ApiOperation(value = "Search for a product by its id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<ProductResponse> findById(@PathVariable("id") @NotEmpty Long id) {
        return ok(this.productService.findById(id));
    }

    @ApiOperation(value = "Search for all products")
    @GetMapping
    public ResponseEntity<List<ProductResponse>> findAll() {
        return ok(this.productService.findAll());
    }

    @ApiOperation(value = "Search for products filtering by the following query parameters: q, min_price, max_price")
    @GetMapping(value = "/search")
    public ResponseEntity<List<ProductResponse>> searchBy(
            @RequestParam(value = "q", required = false) @NotEmpty String q,
            @RequestParam(value = "min_price", required = false) @NotNull BigDecimal minPrice,
            @RequestParam(value = "max_price", required = false) @NotNull BigDecimal maxPrice
    ) {
        return ok(this.productService.searchBy(q, minPrice, maxPrice));
    }

    @ApiOperation(value = "Remove a product by its id")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> remove(@PathVariable("id") Long id) {
        this.productService.remove(id);
        return ok().build();
    }

}
