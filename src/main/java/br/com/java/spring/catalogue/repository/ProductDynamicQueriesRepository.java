package br.com.java.spring.catalogue.repository;

import br.com.java.spring.catalogue.model.Product;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ProductDynamicQueriesRepository {
    List<Product> searchBy(@Param("q") String q, @Param("minPrice") BigDecimal minPrice, @Param("maxPrice") BigDecimal maxPrice);
}
