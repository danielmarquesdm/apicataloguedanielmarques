package br.com.java.spring.catalogue.repository;

import br.com.java.spring.catalogue.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, String>, JpaSpecificationExecutor<Product>, ProductDynamicQueriesRepository {

    @Query("FROM Product p " +
            "WHERE p.id = :id " +
            "AND p.deletedAt is null")
    Optional<Product> getOne(Long id);

    Product save(Product s);
}
