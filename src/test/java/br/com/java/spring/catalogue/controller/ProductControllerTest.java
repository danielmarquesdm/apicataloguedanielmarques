package br.com.java.spring.catalogue.controller;

import br.com.java.spring.catalogue.CatalogueApplication;
import br.com.java.spring.catalogue.builder.ProductBuilder;
import br.com.java.spring.catalogue.builder.ProductRequestBuilder;
import br.com.java.spring.catalogue.builder.ProductResponseBuilder;
import br.com.java.spring.catalogue.controller.request.ProductRequest;
import br.com.java.spring.catalogue.controller.response.ProductResponse;
import br.com.java.spring.catalogue.repository.ProductRepository;
import br.com.java.spring.catalogue.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@SpringBootTest(
        classes = CatalogueApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class ProductControllerTest {
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private ProductService productService;
    @Autowired
    private ProductController productController;

    @Test
    public void mustSaveANewProduct() {
        var requestToSave = new ProductRequestBuilder().build();
        var expectedResponse = new ProductResponseBuilder().build();
        UriComponentsBuilder uri = UriComponentsBuilder.fromUri(URI.create("http://localhost:9999/products"));
        when(this.productService.save(any(ProductRequest.class))).thenReturn(expectedResponse);

        ResponseEntity<ProductResponse> response = this.productController.save(requestToSave, uri);

        assertEquals(expectedResponse, response.getBody());
        assertEquals(CREATED, response.getStatusCode());
        verify(this.productService, times(1)).save(any(ProductRequest.class));
    }

    @Test
    public void mustUpdateAProduct() {
        var productSaved = new ProductResponseBuilder().build();
        var requestToUpdate = new ProductRequestBuilder().withADifferentName("Lore").build();
        var expectedResponse2 = new ProductResponseBuilder().withADifferentName("Lore").build();
        when(this.productService.update(any(ProductRequest.class), anyString())).thenReturn(expectedResponse2);

        ResponseEntity<ProductResponse> actualResponse = this.productController
                .update(requestToUpdate, expectedResponse2.getId());

        assertEquals(expectedResponse2, actualResponse.getBody());
        assertEquals(OK, actualResponse.getStatusCode());
        verify(this.productService, times(1)).update(any(ProductRequest.class), anyString());
    }

    @Test
    public void mustFindByProductId() {
        var productResponse = new ProductResponseBuilder().build();
        when(this.productService.findById(anyLong())).thenReturn(productResponse);

        ResponseEntity<ProductResponse> actualResponse = this.productController
                .findById(Long.parseLong(productResponse.getId()));

        assertEquals(productResponse, actualResponse.getBody());
        assertEquals(OK, actualResponse.getStatusCode());
        verify(this.productService, times(1)).findById(anyLong());
    }

    @Test
    public void mustFindAllProducts() {
        List<ProductResponse> expectedList = new java.util.ArrayList<>();

        var productResponse = new ProductResponseBuilder().build();
        expectedList.add(productResponse);
        productResponse = new ProductResponseBuilder().withAnotherId("2").withADifferentName("Lore").build();
        expectedList.add(productResponse);

        when(this.productService.findAll()).thenReturn(expectedList);
        ResponseEntity<List<ProductResponse>> actualResponse = this.productController.findAll();


        assertEquals(OK, actualResponse.getStatusCode());
        verify(this.productService, times(1)).findAll();
    }

    @Test
    public void mustFilterByQueryParams() {
        var productResponse1 = new ProductResponseBuilder()
                .withAnotherId("1")
                .withADifferentName("name1")
                .withDescription("name1like")
                .withPrice(BigDecimal.ZERO).build();
        var productResponse2 = new ProductResponseBuilder()
                .withAnotherId("2")
                .withADifferentName("name2")
                .withDescription("name2like")
                .withPrice(BigDecimal.ONE).build();
        var productResponse3 = new ProductResponseBuilder()
                .withAnotherId("3")
                .withADifferentName("name1")
                .withDescription("name1Different")
                .withPrice(BigDecimal.TEN).build();
        var productResponse4 = new ProductResponseBuilder()
                .withAnotherId("4")
                .withADifferentName("name4")
                .withDescription("name4")
                .withPrice(BigDecimal.ONE).build();

        List<ProductResponse> expectedList = new java.util.ArrayList<>();
        expectedList.add(productResponse1);
        expectedList.add(productResponse2);
        expectedList.add(productResponse4);

        String qParam = "name";
        BigDecimal minPriceParam = BigDecimal.ZERO;
        BigDecimal maxPriceParam = BigDecimal.ONE;
        when(this.productService.searchBy(anyString(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(expectedList);

        ResponseEntity<List<ProductResponse>> actualResponse = this.productController
                .searchBy(qParam, minPriceParam, maxPriceParam);

        assertEquals(expectedList.get(0), Objects.requireNonNull(actualResponse.getBody()).get(0));
        assertEquals(expectedList.get(1), actualResponse.getBody().get(1));
        assertEquals(expectedList.get(2), actualResponse.getBody().get(2));
        assertEquals(expectedList.size(), actualResponse.getBody().size());
        assertEquals(OK, actualResponse.getStatusCode());
        verify(this.productService, times(1))
                .searchBy(anyString(), any(BigDecimal.class), any(BigDecimal.class));
    }

    @Test
    public void mustDeleteAProduct() {
        var product = new ProductBuilder().build();

        ResponseEntity<Object> actualResponse = this.productController
                .remove(Long.parseLong(product.getId().toString()));

        assertEquals(OK, actualResponse.getStatusCode());
        verify(this.productService, times(1)).remove(anyLong());
    }
}
