package br.com.java.spring.catalogue.builder;

import br.com.java.spring.catalogue.controller.response.ProductResponse;

import java.math.BigDecimal;

public class ProductResponseBuilder {
    private ProductResponse productResponse;

    public ProductResponseBuilder() {
        var productResponse = new ProductResponse();
        productResponse.setId("1");
        productResponse.setName("Wine");
        productResponse.setDescription("White, Dry");
        productResponse.setPrice(BigDecimal.ZERO);
        this.productResponse = productResponse;
    }

    public ProductResponseBuilder withAnotherId(String id) {
        this.productResponse.setId(id);
        return this;
    }

    public ProductResponseBuilder withADifferentName(String name) {
        this.productResponse.setName(name);
        return this;
    }

    public ProductResponseBuilder withDescription(String description) {
        this.productResponse.setDescription(description);
        return this;
    }

    public ProductResponseBuilder withPrice(BigDecimal price) {
        this.productResponse.setPrice(price);
        return this;
    }

    public ProductResponse build() {
        return this.productResponse;
    }
}
