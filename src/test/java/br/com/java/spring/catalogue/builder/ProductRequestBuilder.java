package br.com.java.spring.catalogue.builder;

import br.com.java.spring.catalogue.controller.request.ProductRequest;

import java.math.BigDecimal;

public class ProductRequestBuilder {
    private ProductRequest productRequest;

    public ProductRequestBuilder() {
        var productRequest = new ProductRequest();
        productRequest.setName("Wine");
        productRequest.setDescription("White, Dry");
        productRequest.setName("White, Dry");
        productRequest.setPrice(BigDecimal.ZERO);
        this.productRequest = productRequest;
    }

    public ProductRequestBuilder withNegativeValueForPrice() {
        this.productRequest.setPrice(BigDecimal.valueOf(-0.001));
        return this;
    }

    public ProductRequestBuilder withADifferentName(String name) {
        this.productRequest.setName(name);
        return this;
    }

    public ProductRequest build() {
        return this.productRequest;
    }
}
