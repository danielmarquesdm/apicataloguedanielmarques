package br.com.java.spring.catalogue.service;

import br.com.java.spring.catalogue.CatalogueApplication;
import br.com.java.spring.catalogue.builder.ProductBuilder;
import br.com.java.spring.catalogue.builder.ProductRequestBuilder;
import br.com.java.spring.catalogue.builder.ProductResponseBuilder;
import br.com.java.spring.catalogue.controller.response.ProductResponse;
import br.com.java.spring.catalogue.exception.InvalidRequestException;
import br.com.java.spring.catalogue.exception.ProductNotFoundException;
import br.com.java.spring.catalogue.model.Product;
import br.com.java.spring.catalogue.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.OK;

@SpringBootTest(
        classes = CatalogueApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class ProductServiceTest {
    @MockBean
    private ProductRepository productRepository;
    @Autowired
    private ProductService productService;

    @Test
    public void mustThrowInvalidRequestException() {
        var productRequest = new ProductRequestBuilder().withNegativeValueForPrice().build();

        InvalidRequestException exception = assertThrows(
                InvalidRequestException.class,
                () -> this.productService.save(productRequest),
                "Must return an InvalidRequestException"
        );

        assertNotNull(exception);
        assertTrue(exception.getMessage().contains("Price should have a positive value"));
        verify(productRepository, times(0)).save(any(Product.class));
    }

    @Test
    public void mustSaveANewProduct() {
        var productRequest = new ProductRequestBuilder().build();
        var expectedResponse = new ProductResponseBuilder().build();
        var product = new ProductBuilder().build();
        when(this.productRepository.save(any(Product.class))).thenReturn(product);

        ProductResponse actualResponse = this.productService.save(productRequest);

        assertEquals(expectedResponse, actualResponse);
        verify(productRepository, times(1)).save(any(Product.class));
    }

    @Test
    public void mustUpdateAProduct() {
        var productRequest = new ProductRequestBuilder().build();
        var product = new ProductBuilder().build();
        var productUpdated = new ProductBuilder().withADifferentName("Lore").build();
        when(this.productRepository.getOne(product.getId())).thenReturn(Optional.of(product));
        when(this.productRepository.save(any(Product.class))).thenReturn(productUpdated);

        ProductResponse actualResponse = this.productService.update(productRequest, product.getId().toString());

        assertEquals(productUpdated.getName(), actualResponse.getName());
        verify(productRepository, times(1)).getOne(anyLong());
        verify(productRepository, times(1)).save(any(Product.class));
    }

    @Test
    public void mustFindById() {
        var productExpected = new ProductBuilder().build();
        ProductResponse expectedResponse = ProductResponse.toResponse(productExpected);
        when(this.productRepository.getOne(anyLong())).thenReturn(Optional.of(productExpected));

        ProductResponse actualResponse = this.productService.findById(productExpected.getId());

        assertEquals(expectedResponse, actualResponse);
        verify(productRepository, times(1)).getOne(anyLong());
    }

    @Test
    public void mustThrowProductNotFoundException() {
        ProductNotFoundException exception = assertThrows(
                ProductNotFoundException.class,
                () -> this.productService.findById(1L),
                "Must return an ProductNotFoundException"
        );

        assertNotNull(exception);
        assertTrue(exception.getMessage().contains("No product found for this id"));
        verify(productRepository, times(0)).findById(anyString());
    }

    @Test
    public void mustFindAllProducts() {
        List<Product> expectedList = new java.util.ArrayList<>();
        var product = new ProductBuilder().build();
        expectedList.add(product);
        product = new ProductBuilder().withAnotherId(2L).withADifferentName("Lore").build();
        expectedList.add(product);
        when(this.productRepository.findAll()).thenReturn(expectedList);

        List<ProductResponse> actualResponse = this.productService.findAll();

        assertEquals(ProductResponse.toResponse(expectedList.get(0)), actualResponse.get(0));
        assertEquals(ProductResponse.toResponse(expectedList.get(1)), actualResponse.get(1));
        assertEquals(expectedList.size(), actualResponse.size());
        verify(this.productRepository, times(1)).findAll();
    }

    @Test
    public void mustRemoveAProduct() {
        Product product = new ProductBuilder().build();
        product.setDeletedAt(LocalDateTime.now());
        when(this.productRepository.getOne(anyLong())).thenReturn(Optional.of(product));
        when(this.productRepository.save(any(Product.class))).thenReturn(product);

        this.productService.remove(product.getId());

        verify(this.productRepository, times(1)).save(any(Product.class));
    }

    @Test
    public void mustFilterByQueryParams() {
        var product1 = new ProductBuilder()
                .withAnotherId(1L)
                .withADifferentName("name1")
                .withDescription("name1like")
                .withPrice(BigDecimal.ZERO).build();
        var product2 = new ProductBuilder()
                .withAnotherId(2L)
                .withADifferentName("name2")
                .withDescription("name2like")
                .withPrice(BigDecimal.ONE).build();
        var product3 = new ProductBuilder()
                .withAnotherId(3L)
                .withADifferentName("name1")
                .withDescription("name1Different")
                .withPrice(BigDecimal.TEN).build();
        var product4 = new ProductBuilder()
                .withAnotherId(4L)
                .withADifferentName("name4")
                .withDescription("name4")
                .withPrice(BigDecimal.ONE).build();

        List<Product> expectedList = new java.util.ArrayList<>();
        expectedList.add(product1);
        expectedList.add(product2);
        expectedList.add(product4);

        String qParam = "name";
        BigDecimal minPriceParam = BigDecimal.ZERO;
        BigDecimal maxPriceParam = BigDecimal.ONE;
        when(this.productRepository.searchBy(anyString(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(expectedList);

        List<ProductResponse> actualResponse = this.productService
                .searchBy(qParam, minPriceParam, maxPriceParam);

        assertEquals(ProductResponse.toResponse(expectedList.get(0)), Objects.requireNonNull(actualResponse).get(0));
        assertEquals(ProductResponse.toResponse(expectedList.get(1)), actualResponse.get(1));
        assertEquals(ProductResponse.toResponse(expectedList.get(2)), actualResponse.get(2));
        assertEquals(expectedList.size(), actualResponse.size());
        verify(this.productRepository, times(1))
                .searchBy(anyString(), any(BigDecimal.class), any(BigDecimal.class));
    }
}
