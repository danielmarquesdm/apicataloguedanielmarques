package br.com.java.spring.catalogue.builder;

import br.com.java.spring.catalogue.model.Product;

import java.math.BigDecimal;

public class ProductBuilder {
    private Product product;

    public ProductBuilder() {
        var product = new Product();
        product.setId(1L);
        product.setName("Wine");
        product.setDescription("White, Dry");
        product.setPrice(BigDecimal.ZERO);
        product.setDeletedAt(null);
        this.product = product;
    }

    public ProductBuilder withAnotherId(Long id) {
        this.product.setId(id);
        return this;
    }

    public ProductBuilder withADifferentName(String name) {
        this.product.setName(name);
        return this;
    }

    public ProductBuilder withDescription(String description) {
        this.product.setDescription(description);
        return this;
    }

    public ProductBuilder withPrice(BigDecimal price) {
        this.product.setPrice(price);
        return this;
    }

    public Product build() {
        return this.product;
    }
}
